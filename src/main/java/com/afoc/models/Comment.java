package com.afoc.models;

import org.jetbrains.annotations.NotNull;

public class Comment implements Comparable<Comment>{

    private String post_id;
    private String name;
    private String email;
    private String body;

    public Comment(String post_id, String name, String email, String body) {
        this.post_id = post_id;
        this.name = name;
        this.email = email;
        this.body = body;
    }

    public String getPost_id() {
        return post_id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getBody() {
        return body;
    }

    @Override
    public int compareTo(@NotNull Comment comment) {
        if (comment.getPost_id().equals(post_id) &&
                comment.getName().equals(name) &&
                comment.getEmail().equals(email) &&
                comment.getBody().equals(body)) {
            return 0;
        }
        return -1;
    }
}
