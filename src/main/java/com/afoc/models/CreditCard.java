package com.afoc.models;

public class CreditCard {
    private String numberId;
    private String mouthCounts;
    private String firstName;
    private String lastName;
    private String typeId;
    private String expDate;
    private String cvv;
    private String cardNumber;

    public CreditCard(String numberId, String mouthCounts, String firstName, String lastName, String typeId, String expDate, String cvv, String cardNumber) {
        this.numberId = numberId;
        this.mouthCounts = mouthCounts;
        this.firstName = firstName;
        this.lastName = lastName;
        this.typeId = typeId;
        this.expDate = expDate;
        this.cvv = cvv;
        this.cardNumber = cardNumber;
    }

    public String getNumberId() {
        return numberId;
    }

    public String getMouthCounts() {
        return mouthCounts;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getTypeId() {
        return typeId;
    }

    public String getExpDate() {
        return expDate;
    }

    public String getCvv() {
        return cvv;
    }

    public String getCardNumber() {
        return cardNumber;
    }
}
