package com.afoc.models;

public class IdUser {

    private String id;

    public IdUser(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
