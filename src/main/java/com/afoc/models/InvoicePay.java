package com.afoc.models;

public class InvoicePay {

    private String firstName;
    private String lastName;
    private String typeIdInvoice;
    private String numberId;
    private String address;
    private String numberPhone;

    public InvoicePay(String firstName, String lastName, String typeIdInvoice, String numberId, String address, String numberPhone) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.typeIdInvoice = typeIdInvoice;
        this.numberId = numberId;
        this.address = address;
        this.numberPhone = numberPhone;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getTypeIdInvoice() {
        return typeIdInvoice;
    }

    public String getNumberId() {
        return numberId;
    }

    public String getAddress() {
        return address;
    }

    public String getNumberPhone() {
        return numberPhone;
    }
}
