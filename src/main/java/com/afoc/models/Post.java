package com.afoc.models;


import com.sun.istack.NotNull;

public class Post implements Comparable<Post> {

    private String user_id;
    private String title;
    private String body;

    public Post(String user_id, String title, String body) {
        this.user_id = user_id;
        this.title = title;
        this.body = body;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    @Override
    public int compareTo(@NotNull Post post) {
        if (post.getUser_id().equals(user_id) &&
                post.getTitle().equals(title) &&
                post.getBody().equals(body)) {
            return 0;
        }
        return -1;
    }
}
