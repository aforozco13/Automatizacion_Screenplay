package com.afoc.models;

public class Reserver {
    private String selected;
    private String zoneArrive;
    private String dateInit;
    private String dateFinish;
    private String selectDiscount;
    private String codePromotional;

    public Reserver(String selected, String zoneArrive, String dateInit, String dateFinish, String selectDiscount, String codePromotional) {
        this.selected = selected;
        this.zoneArrive = zoneArrive;
        this.dateInit = dateInit;
        this.dateFinish = dateFinish;
        this.selectDiscount = selectDiscount;
        this.codePromotional = codePromotional;
    }

    public String getSelected() {
        return selected;
    }

    public String getZoneArrive() {
        return zoneArrive;
    }

    public String getDateInit() {
        return dateInit;
    }

    public String getDateFinish() {
        return dateFinish;
    }

    public String getSelectDiscount() {
        return selectDiscount;
    }

    public String getCodePromotional() {
        return codePromotional;
    }
}
