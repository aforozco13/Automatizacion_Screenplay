package com.afoc.models;

public class TravelInfo {

    private String gender;
    private String firstName;
    private String lastName;
    private String typeId;
    private String numberId;
    private String birthDate;
    private String numberPhone;
    private String email;
    private String commit;

    public TravelInfo(String gender, String firstName, String lastName, String typeId, String numberId, String birthDate, String numberPhone, String email, String commit) {
        this.gender = gender;
        this.firstName = firstName;
        this.lastName = lastName;
        this.typeId = typeId;
        this.numberId = numberId;
        this.birthDate = birthDate;
        this.numberPhone = numberPhone;
        this.email = email;
        this.commit = commit;
    }

    public String getGender() {
        return gender;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getTypeId() {
        return typeId;
    }

    public String getNumberId() {
        return numberId;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public String getNumberPhone() {
        return numberPhone;
    }

    public String getEmail() {
        return email;
    }

    public String getCommit() {
        return commit;
    }
}
