package com.afoc.models;

import org.jetbrains.annotations.NotNull;

public class User implements Comparable<User> {
    private String name;
    private String gender;
    private String email;
    private String status;

    public User(String name, String email, String gender, String status) {
        this.name = name;
        this.gender = gender;
        this.email = email;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public String getEmail() {
        return email;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", gender='" + gender + '\'' +
                ", status='" + status + '\'' +
                '}';
    }

    @Override
    public int compareTo(@NotNull User user) {
        if (user.getName().equals(name) &&
                user.getEmail().equals(email) &&
                user.getGender().equals(gender) &&
                user.getStatus().equals(status)) {
            return 0;
        }
        return -1;
    }
}
