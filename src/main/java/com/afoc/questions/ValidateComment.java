package com.afoc.questions;

import com.afoc.models.Comment;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;



public class ValidateComment implements Question<Comment> {
    @Override
    public Comment answeredBy(Actor actor) {
        return SerenityRest.lastResponse().jsonPath().getObject("data", Comment.class);
    }

    public static ValidateComment toComment() {
        return new ValidateComment();
    }
}
