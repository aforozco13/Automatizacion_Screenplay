package com.afoc.questions;

import com.afoc.models.Post;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class ValidatePosts implements Question<Post> {

    @Override
    public Post answeredBy(Actor actor) {
        return SerenityRest.lastResponse().jsonPath().getObject("data", Post.class);
    }

    public static ValidatePosts toPost() {
        return new ValidatePosts();
    }
}
