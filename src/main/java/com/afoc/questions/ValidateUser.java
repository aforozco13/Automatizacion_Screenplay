package com.afoc.questions;

import com.afoc.models.User;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class ValidateUser implements Question<User> {
    @Override
    public User answeredBy(Actor actor) {
        return SerenityRest.lastResponse().jsonPath().getObject("data", User.class);
    }

    public static ValidateUser toUser() {
        return new ValidateUser();
    }
}
