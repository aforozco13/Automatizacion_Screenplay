package com.afoc.tasks;

import com.afoc.models.Reserver;
import io.cucumber.datatable.DataTable;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

import java.util.List;

import static com.afoc.userinterface.DestinoJetHomePage.BTN_CARS;

public class DestinoJestHome implements Task {

    private List<Reserver> reservers;

    public DestinoJestHome(DataTable data) {
        super();
        this.reservers = data.asList(Reserver.class);
    }

    public static DestinoJestHome select(DataTable data) { return Tasks.instrumented(DestinoJestHome.class, data);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(BTN_CARS.of(reservers.get(0).getSelected())));
    }
}
