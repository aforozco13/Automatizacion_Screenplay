package com.afoc.tasks;

import com.afoc.models.CreditCard;
import io.cucumber.datatable.DataTable;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

import java.util.List;

import static com.afoc.userinterface.DestinoJetCardPage.*;
import static com.afoc.utils.Util.dataOrder;

public class DestinoJetCreditCard implements Task {
    List<CreditCard> creditCards;

    public DestinoJetCreditCard(DataTable data) {
        this.creditCards = data.asList(CreditCard.class);
    }

    public static DestinoJetCreditCard payToCard(DataTable data) {
        return Tasks.instrumented(DestinoJetCreditCard.class, data);
    }


    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(creditCards.get(0).getFirstName()).into(FIRST_NAME_CARD),
                Enter.theValue(creditCards.get(0).getLastName()).into(LAST_NAME_CARD),
                SelectFromOptions.byVisibleText(creditCards.get(0).getTypeId()).from(TYPE_ID_CARD),
                Enter.theValue(creditCards.get(0).getNumberId()).into(ID_CARD),
                SelectFromOptions.byVisibleText(creditCards.get(0).getMouthCounts()).from(FEES_CARD),
                SelectFromOptions.byVisibleText(dataOrder(creditCards.get(0).getExpDate())[0]).from(EXP_MOUNTH),
                SelectFromOptions.byVisibleText(dataOrder(creditCards.get(0).getExpDate())[1]).from(EXP_YEAR),
                Enter.theValue(creditCards.get(0).getCvv()).into(CVV),
                Enter.theValue(creditCards.get(0).getCardNumber()).into(NUMBER_CARD),
                Click.on(RESERVER),
                Click.on(BTN_RESERVE)
        );
    }
}
