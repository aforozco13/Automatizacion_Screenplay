package com.afoc.tasks;

import com.afoc.models.InvoicePay;
import io.cucumber.datatable.DataTable;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

import java.util.List;

import static com.afoc.userinterface.DestinoJetInvoicePage.*;

public class DestinoJetInvoice implements Task {

    private List<InvoicePay> invoicePays;

    public DestinoJetInvoice(DataTable data) {
        super();
        this.invoicePays = data.asList(InvoicePay.class);
    }

    public static DestinoJetInvoice invoice(DataTable data) {
        return Tasks.instrumented(DestinoJetInvoice.class, data);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(invoicePays.get(0).getFirstName()).into(FIRST_NAME_PAY),
                Enter.theValue(invoicePays.get(0).getLastName()).into(LAST_NAME_PAY),
                SelectFromOptions.byVisibleText(invoicePays.get(0).getTypeIdInvoice()).from(TYPE_ID_PAY),
                Enter.theValue(invoicePays.get(0).getNumberId()).into(ID_PAY),
                Enter.theValue(invoicePays.get(0).getAddress()).into(ADDRESS),
                Enter.theValue(invoicePays.get(0).getNumberPhone()).into(NUM_PHONE_PAY)
        );
    }
}
