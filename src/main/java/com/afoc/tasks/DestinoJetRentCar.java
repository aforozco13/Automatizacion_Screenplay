package com.afoc.tasks;

import com.afoc.models.Reserver;
import io.cucumber.datatable.DataTable;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.*;
import org.openqa.selenium.Keys;


import java.util.List;

import static com.afoc.userinterface.DestinoJetSelectOptionPage.*;

import static com.afoc.utils.Util.dataOrderWord;

public class DestinoJetRentCar implements Task {
    private List<Reserver> reservers;

    public DestinoJetRentCar(DataTable data) {
        super();
        this.reservers = data.asList(Reserver.class);
    }

    public static DestinoJetRentCar rent(DataTable data) {
        return Tasks.instrumented(DestinoJetRentCar.class, data);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        String city = dataOrderWord(reservers.get(0).getZoneArrive())[0];
        for (int i = 1; i < reservers.get(0).getZoneArrive().length(); i++) {
            city = city.concat(dataOrderWord(reservers.get(0).getZoneArrive())[i]);
            actor.attemptsTo(
                    Enter.theValue(city).into(SELECT_CITY)
            );
        }

        actor.attemptsTo(
                //WaitUntil.the(OPTIONS_CITY, isVisible()).forNoMoreThan(30).seconds(),
                Hit.the(Keys.TAB).keyIn(SELECT_CITY),
                Enter.theValue(reservers.get(0).getDateInit()).into(DATE_ARRIVE),
                Hit.the(Keys.TAB).keyIn(DATE_ARRIVE),
                Enter.theValue(reservers.get(0).getDateInit()).into(DATE_RETURN),
                Hit.the(Keys.TAB).keyIn(DATE_RETURN),
                //SelectFromOptions.byVisibleText(reservers.get(0).getSelectDiscount()).from(CARD_OFF),
                //SelectFromOptions.byVisibleText(reservers.get(0).getCodePromotional()).from(CODE_PROM),
                Hit.the(Keys.ENTER).keyIn(BTN_SEARCH),
                //Click.on(BTN_SEARCH),
                Click.on(STANDAR_VEHICLE),
                Click.on(MOST_SELLER)
        );
    }
}
