package com.afoc.tasks;

import com.afoc.models.TravelInfo;
import io.cucumber.datatable.DataTable;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

import java.util.List;

import static  com.afoc.utils.Util.*;
import static com.afoc.userinterface.DestinoJetTravelPage.*;

public class DestinoJetTravelInfo implements Task {

    private List<TravelInfo> travelInfos;

    public DestinoJetTravelInfo(DataTable data) {
        super();
        this.travelInfos = data.asList(TravelInfo.class);
    }

    public static DestinoJetTravelInfo travels(DataTable data) {
        return Tasks.instrumented(DestinoJetTravelInfo.class, data);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                SelectFromOptions.byVisibleText(travelInfos.get(0).getGender()).from(GENDER),
                Enter.theValue(travelInfos.get(0).getFirstName()).into(FIRST_NAME_TRAVEL),
                Enter.theValue(travelInfos.get(0).getLastName()).into(LAST_NAME_TRAVEL),
                SelectFromOptions.byVisibleText(travelInfos.get(0).getTypeId()).from(TYPE_ID),
                Enter.theValue(travelInfos.get(0).getTypeId()).into(NUMBER_ID),
                SelectFromOptions.byVisibleText(dataOrder(travelInfos.get(0).getBirthDate())[0]).from(BIRTH_MOUNTH),
                SelectFromOptions.byVisibleText(dataOrder(travelInfos.get(0).getBirthDate())[1]).from(BIRTH_DAY),
                SelectFromOptions.byVisibleText(dataOrder(travelInfos.get(0).getBirthDate())[2]).from(BIRTH_YEAR),
                Enter.theValue(aleatoryNumber(travelInfos.get(0).getEmail())).into(EMAIL),
                Enter.theValue(travelInfos.get(0).getNumberPhone()).into(NUMBER_PHONE),
                Enter.theValue(travelInfos.get(0).getCommit()).into(COMMIT)
        );
    }
}
