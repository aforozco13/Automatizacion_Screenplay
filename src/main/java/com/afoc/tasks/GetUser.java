package com.afoc.tasks;


import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Get;

import static com.afoc.utils.Constant.TOKEN;

public class GetUser implements Task {
    private String endPoint;
    private String id;

    public GetUser(String endPoint, String id) {
        this.endPoint = endPoint;
        this.id = id;
    }

    public static GetUser consume (String endPoint, String id){
        return Tasks.instrumented(GetUser.class, endPoint, id);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Get.resource(endPoint.concat(id)).with(request -> request.auth().oauth2(TOKEN))
        );
        SerenityRest.lastResponse().prettyPeek();
    }


}
