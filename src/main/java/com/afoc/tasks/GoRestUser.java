package com.afoc.tasks;

import com.afoc.models.CodeUser;
import com.afoc.models.DataUser;
import com.afoc.models.IdUser;
import io.cucumber.datatable.DataTable;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;


import java.util.List;

import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.core.IsEqual.equalTo;

public class GoRestUser implements Task {

    List<DataUser> dataUser;
    List<CodeUser> codeUser;
    List<IdUser> idUser;

    public GoRestUser(DataTable data) {
        dataUser = data.asList(DataUser.class);
        codeUser = data.asList(CodeUser.class);
        idUser = data.asList(IdUser.class);
    }

    public static GoRestUser in(DataTable data) {
        return Tasks.instrumented(GoRestUser.class,data);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.should(seeThatResponse(response ->
                response.statusCode(Integer.parseInt(codeUser.get(0).getCodigo()))
                        .body("data.id", equalTo(Integer.parseInt(idUser.get(0).getId())))
                        .body("data.name", equalTo(dataUser.get(0).getName()))
                        .body("data.email", equalTo(dataUser.get(0).getEmail()))
                        .body("data.gender", equalTo(dataUser.get(0).getGender()))
                        .body("data.status", equalTo(dataUser.get(0).getStatus()))
        ));
        SerenityRest.lastResponse().prettyPeek();
        System.out.println("--------------> Datos validados correctamente<--------------");

    }
}
