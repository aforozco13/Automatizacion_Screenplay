package com.afoc.tasks;

import com.afoc.utils.Constant;
import io.restassured.http.ContentType;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Post;
import org.apache.hc.core5.http.HttpHeaders;

import java.util.Map;


public class PostRecurso implements Task {

    private String endPoint;
    private Map<String, String> recursos;

    public PostRecurso(String endPoint, Map<String, String> recursos) {
        this.endPoint = endPoint;
        this.recursos = recursos;
    }

    public static PostRecurso postRecursoFill(String endPoint, Map<String, String> recursos) {
        return Tasks.instrumented(PostRecurso.class, endPoint, recursos);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Post.to(endPoint).with(request ->
                        request.header(HttpHeaders.CONTENT_TYPE, ContentType.JSON).auth().oauth2(Constant.TOKEN).body(recursos))
        );
    }
}
