package com.afoc.userinterface;

import net.serenitybdd.screenplay.targets.Target;

public class DestinoJetCardPage {

    public static final Target FIRST_NAME_CARD = Target.the("Campo para ingresar el nombre en la tarjeta").locatedBy("id:name");
    public static final Target LAST_NAME_CARD = Target.the("Campo para ingresar el apellido en la tarjeta").locatedBy("id:lastname");
    public static final Target TYPE_ID_CARD = Target.the("Botón para seleccionar el tipo de documento en la tarjeta").locatedBy("id:documentType");
    public static final Target ID_CARD = Target.the("Campo para ingresar la cedula en la tarjeta").locatedBy("(//*[@id='employeeId'])[1]");
    public static final Target FEES_CARD = Target.the("Campo para ingresar cuotas").locatedBy("id:installments");
    public static final Target EXP_MOUNTH = Target.the("Boton para seleccionar el mes de vencimiento en la tarjeta").locatedBy("id:expirationMonth");
    public static final Target EXP_YEAR = Target.the("Boton para seleccionar el año de vencimiento en la tarjeta").locatedBy("id:expirationYear");
    public static final Target CVV = Target.the("Campo para ingresar el cvv").locatedBy("id:cvc");
    public static final Target NUMBER_CARD = Target.the("Campo para ingresar el numero de la tarjeta").locatedBy("id:number");
    public static final Target RESERVER = Target.the("Boton para continuar la reserva").locatedBy("//*[@class='carsResume__action']");
    public static final Target BTN_RESERVE = Target.the("Boton para confirmar la reserva").locatedBy("//*[@class='confirm']");

}
