package com.afoc.userinterface;

import net.serenitybdd.screenplay.targets.Target;

public class DestinoJetConfirmacion {
    public static final Target CONFIRMATION = Target.the("Element to confirm is success reservation").locatedBy("//*[text()='Tarifa aplicada']");

}
