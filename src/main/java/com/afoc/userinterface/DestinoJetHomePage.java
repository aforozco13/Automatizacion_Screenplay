package com.afoc.userinterface;


import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://test.destinojet.co/")
public class DestinoJetHomePage extends PageObject {
    public static final Target BTN_CARS = Target.the("Boton for select to car").locatedBy("//*[@class='prominent-product-action' and @data-product='{0}']");
}
