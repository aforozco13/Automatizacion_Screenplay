package com.afoc.userinterface;

import net.serenitybdd.screenplay.targets.Target;

public class DestinoJetInvoicePage {

    public static final Target FIRST_NAME_PAY = Target.the("Campo para ingresar el nombre de facturacion").locatedBy("id:FirstName");
    public static final Target LAST_NAME_PAY = Target.the("Campo para ingresar el apellido de facturacion").locatedBy("id:LastName");
    public static final Target TYPE_ID_PAY = Target.the("Botón para seleccionar el tipo de documento en la tarjeta").locatedBy("id:DocumentType");
    public static final Target ID_PAY = Target.the("Campo para ingresar la cedula de la tarjeta").locatedBy("id:CustomerId");
    public static final Target ADDRESS = Target.the("Campo para ingresar la direccion de facturacion").locatedBy("id:Address");
    public static final Target NUM_PHONE_PAY = Target.the("Campo para ingresar el telefono de facturacion").locatedBy("id:Telephone");
}
