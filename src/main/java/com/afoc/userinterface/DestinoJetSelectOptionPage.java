package com.afoc.userinterface;

import net.serenitybdd.screenplay.targets.Target;

public class DestinoJetSelectOptionPage {

    public static final Target SELECT_CITY = Target.the("Select the gender for the travels").locatedBy("id:PickUpLocation");
    public static final Target DATE_ARRIVE = Target.the("Added the first name of travels").locatedBy("id:PickUpDate");
    public static final Target DATE_RETURN = Target.the("Added the last name of travels").locatedBy("id:DropOffDate");
    public static final Target OPTIONS_CITY = Target.the("Options city").locatedBy("//*[@id='PickUpLocation_Gmt']/span[@class='twitter-typeahead']/div");
    public static final Target CARD_OFF = Target.the("Select type of of identifiquer card").locatedBy("id:corporateDiscount");
    public static final Target CODE_PROM = Target.the("Write the number id").locatedBy("id:promotionalCode");
    public static final Target BTN_SEARCH = Target.the("Select date of mount birt").locatedBy("id:cars-trigger");
    public static final Target STANDAR_VEHICLE = Target.the("Select date of day birth").locatedBy("//a[@class='cars-grid-tab']//ancestor::*[text()='Standard']");
    public static final Target MOST_SELLER = Target.the("Select date of year birth").locatedBy("(//*[@data-care='2'])[4]");
}
