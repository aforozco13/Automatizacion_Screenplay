package com.afoc.userinterface;


import net.serenitybdd.screenplay.targets.Target;

public class DestinoJetTravelPage {
    public static final Target GENDER = Target.the("Select the gender for the travels").locatedBy("id:Gender-Adult-0");
    public static final Target FIRST_NAME_TRAVEL = Target.the("Added the first name of travels").locatedBy("id:FirstName-Adult-0");
    public static final Target LAST_NAME_TRAVEL = Target.the("Added the last name of travels").locatedBy("id:LastName-Adult-0");
    public static final Target TYPE_ID = Target.the("Select type of of identifiquer card").locatedBy("id:DocumentType-Adult-0");
    public static final Target NUMBER_ID = Target.the("Write the number id").locatedBy("id:EmployeeId-Adult-0");
    public static final Target BIRTH_MOUNTH = Target.the("Select date of mount birt").locatedBy("id:Moth-Adult-0");
    public static final Target BIRTH_DAY = Target.the("Select date of day birth").locatedBy("id:Day-Adult-0");
    public static final Target BIRTH_YEAR = Target.the("Select date of year birth").locatedBy("id:Year-Adult-0");
    public static final Target EMAIL = Target.the("Added the email of registration").locatedBy("id:Email");
    public static final Target NUMBER_PHONE = Target.the("Add the number phone for reserved a car").locatedBy("id:PhoneNumber");
    public static final Target COMMIT = Target.the("Add more commit for reserved").locatedBy("id:coemntario");
}
