package com.afoc.utils;

import java.util.Random;

public class Util {
    public static String[] dataOrder(String data) {
        return data.split("-");
    }
    public static String[] dataOrderWord(String data) {
        return data.split("");
    }
    public static String aleatoryNumber(String data) {
        return new Random().nextInt() + data;
    }
}
