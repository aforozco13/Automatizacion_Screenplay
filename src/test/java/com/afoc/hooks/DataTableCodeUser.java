package com.afoc.hooks;

import com.afoc.models.CodeUser;
import io.cucumber.java.DataTableType;

import java.util.Map;

public class DataTableCodeUser {
    @DataTableType
    public CodeUser transformToCodeUser(Map<String, String> entry) {
        return new CodeUser(
                entry.get("codigo")
        );
    }
}
