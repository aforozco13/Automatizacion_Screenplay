package com.afoc.hooks;

import com.afoc.models.Comment;
import io.cucumber.java.DataTableType;

import java.util.Map;

public class DataTableComment {

    @DataTableType
    public Comment transformToComment(Map<String, String> data) {
        return new Comment(
                data.get("post_id"),
                data.get("name"),
                data.get("email"),
                data.get("body")
        );
    }
}
