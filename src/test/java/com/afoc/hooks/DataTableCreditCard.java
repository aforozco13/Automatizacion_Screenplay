package com.afoc.hooks;

import com.afoc.models.CreditCard;
import io.cucumber.java.DataTableType;

import java.util.Map;

public class DataTableCreditCard {

    @DataTableType
    public CreditCard transformToCreditCard(Map<String, String> entry) {
        return new CreditCard(
                entry.get("numberId"),
                entry.get("mouthCounts"),
                entry.get("firstName"),
                entry.get("lastName"),
                entry.get("typeId"),
                entry.get("expDate"),
                entry.get("cvv"),
                entry.get("cardNumber")
        );
    }
}
