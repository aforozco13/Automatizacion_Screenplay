package com.afoc.hooks;

import com.afoc.models.DataUser;
import io.cucumber.java.DataTableType;

import java.util.Map;

public class DataTableDataUser {

    @DataTableType
    public DataUser transformToDataUser(Map<String, String> entry) {
        return new DataUser(
                entry.get("name"),
                entry.get("gender"),
                entry.get("email"),
                entry.get("status")
        );
    }

}
