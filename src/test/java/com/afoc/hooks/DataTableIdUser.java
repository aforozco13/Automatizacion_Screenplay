package com.afoc.hooks;

import com.afoc.models.IdUser;
import io.cucumber.java.DataTableType;

import java.util.Map;

public class DataTableIdUser {
    @DataTableType
    public IdUser transformToIdUser(Map<String, String> entry) { return new IdUser(entry.get("id"));}
}
