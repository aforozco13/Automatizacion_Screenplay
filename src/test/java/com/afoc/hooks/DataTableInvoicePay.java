package com.afoc.hooks;


import com.afoc.models.InvoicePay;
import io.cucumber.java.DataTableType;

import java.util.Map;

public class DataTableInvoicePay {
    @DataTableType
    public InvoicePay transformToInvoicePay(Map<String, String> entry) {
        return new InvoicePay(
                entry.get("firstName"),
                entry.get("lastName"),
                entry.get("typeIdInvoice"),
                entry.get("numberId"),
                entry.get("address"),
                entry.get("numberPhone")
        );
    }
}
