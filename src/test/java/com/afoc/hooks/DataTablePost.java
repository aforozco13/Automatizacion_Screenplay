package com.afoc.hooks;

import com.afoc.models.Post;
import io.cucumber.java.DataTableType;

import java.util.Map;

public class DataTablePost {

    @DataTableType
    public Post transformToPosts(Map<String, String> data) {
        return new Post(
                    data.get("user_id"),
                    data.get("title"),
                    data.get("body")
        );
    }
}
