package com.afoc.hooks;

import com.afoc.models.Reserver;
import io.cucumber.java.DataTableType;

import java.util.Map;

public class DataTableReserver {

    @DataTableType
    public Reserver transformToReserver(Map<String, String> entry) {
        return new Reserver(
                entry.get("selected"),
                entry.get("zoneArrive"),
                entry.get("dateInit"),
                entry.get("dateFinish"),
                entry.get("selectDiscount"),
                entry.get("codePromotional")

        );
    }
}
