package com.afoc.hooks;

import com.afoc.models.TravelInfo;
import io.cucumber.java.DataTableType;

import java.util.Map;

public class DataTableTravelInfo {
    @DataTableType
    public TravelInfo transformToTravelInfo(Map<String, String> entry) {
        return new TravelInfo(
                entry.get("gender"),
                entry.get("firstName"),
                entry.get("lastName"),
                entry.get("typeId"),
                entry.get("numberId"),
                entry.get("birthDate"),
                entry.get("numberPhone"),
                entry.get("email"),
                entry.get("commit")
        );
    }
}
