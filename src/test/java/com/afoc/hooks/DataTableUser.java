package com.afoc.hooks;

import com.afoc.models.User;
import io.cucumber.java.DataTableType;

import java.util.Map;

public class DataTableUser {
    @DataTableType
    public User transformToUser(Map<String, String> entry) {
        return new User(
                entry.get("name"),
                entry.get("email"),
                entry.get("gender"),
                entry.get("status")
        );
    }
}