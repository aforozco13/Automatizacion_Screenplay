package com.afoc.stepdefinitions;

import com.afoc.tasks.*;
import com.afoc.userinterface.DestinoJetHomePage;
import com.afoc.utils.Errors;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.actions.Open;

import static com.afoc.userinterface.DestinoJetConfirmacion.CONFIRMATION;
import static net.serenitybdd.screenplay.actors.OnStage.*;
public class DestinoJetStepDefinition {

    @Given("{string} desea reservar un automovil en destinoJet")
    public void deseaReservarUnAutomovilEnDestinoJet(String actorName) {
        theActorCalled(actorName).wasAbleTo(Open.browserOn(new DestinoJetHomePage()));
    }

    @When("Ingresa los datos iniciales para la reserva")
    public void ingresaLosDatosInicialesParaLaReserva(io.cucumber.datatable.DataTable dataTable) {
        theActorInTheSpotlight().attemptsTo(
                DestinoJestHome.select(dataTable),
                DestinoJetRentCar.rent(dataTable)
        );
    }

    @When("Rellena los datos para el pago")
    public void rellenaLosDatosParaElPago(io.cucumber.datatable.DataTable dataTable) {
        theActorInTheSpotlight().attemptsTo(
                DestinoJetTravelInfo.travels(dataTable),
                DestinoJetInvoice.invoice(dataTable),
                DestinoJetCreditCard.payToCard(dataTable)
        );
    }

    @Then("Valida que el mensaje de la compra sea {string}")
    public void validaQueElMensajeDeLaCompraSea(String msg) {
        Errors.setErrors(CONFIRMATION, msg);
    }


}
