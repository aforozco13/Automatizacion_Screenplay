package com.afoc.stepdefinitions;

import com.afoc.models.Comment;
import com.afoc.models.Post;
import com.afoc.models.User;
import com.afoc.questions.ValidateComment;
import com.afoc.questions.ValidatePosts;
import com.afoc.questions.ValidateUser;
import com.afoc.tasks.PostRecurso;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.actors.OnStage;


import java.util.Map;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.Matchers.comparesEqualTo;

public class GoRestPostStepDefinition {
    @When("Consume el endPont {string} con la instrucción POST")
    public void consumeElEndPontConLaInstrucciónPOST(String endPoint, Map<String, String> data) {
        OnStage.theActorInTheSpotlight().attemptsTo(
                PostRecurso.postRecursoFill(endPoint, data)
        );
    }

    @Then("Verifica que la información coincida {int}")
    public void verificaQueLaInformaciónCoincida(int code, User user) {
        OnStage.theActorInTheSpotlight().should(
                seeThatResponse(response -> response.statusCode(code)),
                seeThat(ValidateUser.toUser(), comparesEqualTo(user))
        );
    }

    @Then("Verifica que la información del post coincida {int}")
    public void verificaQueLaInformaciónDelPostCoincida(int code, Post post) {
        OnStage.theActorInTheSpotlight().should(
                seeThatResponse(response -> response.statusCode(code)),
                seeThat(ValidatePosts.toPost(), comparesEqualTo(post))
        );

    }

    @Then("Verifica que la información del comentario coincida {int}")
    public void verificaQueLaInformaciónDelComentarioCoincida(int code, Comment comment) {
        OnStage.theActorInTheSpotlight().should(
                seeThatResponse(response -> response.statusCode(code)),
                seeThat(ValidateComment.toComment(), comparesEqualTo(comment))
        );

    }

}
