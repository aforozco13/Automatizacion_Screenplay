package com.afoc.stepdefinitions;

import com.afoc.models.IdUser;
import com.afoc.tasks.GetUser;
import com.afoc.tasks.GoRestUser;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class GoRestStepDefinition {
    @Given("{string} desea construir la peticion en gorest {string}")
    public void deseaConstruirLaPeticionEnGorest(String actorName, String baseUrl) {
        theActorCalled(actorName).whoCan(CallAnApi.at(baseUrl));
    }

    @When("Consume el endPont {string} con la instrucción GET y el id")
    public void consumeElEndPontConLaInstrucciónGETYElId(String endPoint, IdUser id) {
        theActorInTheSpotlight().attemptsTo(GetUser.consume(endPoint, id.getId()));
    }

    @Then("Verifica que la información coincida")
    public void verificaQueLaInformaciónCoincida(DataTable dataTable) {
        theActorInTheSpotlight().attemptsTo(GoRestUser.in(dataTable));
    }
}
