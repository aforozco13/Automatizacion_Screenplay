#Author: afoc
Feature: Comprar tickest ida y vuelta
  Yo como usuario
  Necesito realizar la compra de tickets ida y vuelta en avianca
  Para irme de vacaciones

  Scenario Outline: Comprar tickets ida y vuelta
    Given "Afoc" desea comprar tickets ida y vuelta en avianca <url>
    When Ingresa los datos el viaje
      | origen   | destino   | fechaIda   | fechaVuelta   | numAdultos   | numNinios   | numBebes   | clase   | categoria   |
      | <origen> | <destino> | <fechaIda> | <fechaVuelta> | <numAdultos> | <numNinios> | <numBebes> | <clase> | <categoria> |
    And Selecciona el ticket de ida, de vuelta y llena la informacion personal
      | nombre   | apellido   | correo   | paisArea   | numTelefono   |
      | <nombre> | <apellido> | <correo> | <paisArea> | <numTelefono> |
    And Llena la informacion de pago
      | numTarjeta   | fechaVenci   | cvv   | numCuotas   | nombretitular   | tipoDocuTitular   | numDocuTitular   | correoTitular   | paisAreaTitular   | numTelefonoTitular   | ciudadFactura   | provinciaFactura   | direccionFactura   |
      | <numTarjeta> | <fechaVenci> | <cvv> | <numCuotas> | <nombretitular> | <tipoDocuTitular> | <numDocuTitular> | <correoTitular> | <paisAreaTitular> | <numTelefonoTitular> | <ciudadFactura> | <provinciaFactura> | <direccionFactura> |
    Then Valida que el mensaje de la compra de ticktes sea <msg>

    Examples:
      | origen     | destino      | fechaIda  | fechaVuelta | numAdultos | numNinios | numBebes | clase | categoria | nombre | apellido | correo          | paisArea       | numTelefono | numTarjeta       | fechaVenci | cvv | numCuotas | nombretitular | tipoDocuTitular | numDocuTitular | correoTitular   | paisAreaTitular | numTelefonoTitular | ciudadFactura | provinciaFactura | direccionFactura | msg           |
      | Valledupar | Barranquilla | 2022.8.11 | 2022.8.20   | 3          | 2         | 1        | 1     | S         | Pool   | Lopez    | polo@prueba.com | Colombia (+57) | 123456789   | 5198924302640265 | 07-24      | 529 | 1         | Pool Lopez    | C               | 1065789458     | polo@prueba.com | Colombia (+57)  | 123456789          | Valledupar    | Cesar            | calle 90         | Confirmación: |