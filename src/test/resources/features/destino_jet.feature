#Author: afoc
Feature: Reservar un auto en la pagina destino jet
  Yo como usuario
  Necesito realizar la reserva de un automovil
  Para transportarme

  Scenario Outline: Reservar un vehiculo de forma exitosa
    Given "Afoc" desea reservar un automovil en destinoJet
    When Ingresa los datos iniciales para la reserva
      | selected   | zoneArrive   | dateInit   | dateFinish   | SelectDiscount   | codePromotional   | typeCar   |
      | <selected> | <zoneArrive> | <dateInit> | <dateFinish> | <SelectDiscount> | <codePromotional> | <typeCar> |
    And Rellena los datos para el pago
      | gender   | firstName   | lastName   | typeId   | birthDate   | numberPhone   | email   | commit   | address   | mouthCounts   | expDate   | cvv   | cardNumber   | typeIdInvoice   |
      | <gender> | <firstName> | <lastName> | <typeId> | <birthDate> | <numberPhone> | <email> | <commit> | <address> | <mouthCounts> | <expDate> | <cvv> | <cardNumber> | <typeIdInvoice> |
    Then Valida que el mensaje de la compra sea "<msg>"

    Examples:
      | selected | zoneArrive | dateInit  | dateFinish | SelectDiscount                        | codePromotional                  | typeCar  | gender    | firstName | lastName | typeId               | typeIdInvoice         | birthDate     | numberPhone | email        | commit             | address  | mouthCounts | expDate | cvv | cardNumber       | msg           |
      | Car      | Barcelona  | 2022-08-11 | 2022-08-20  | Descuento TarjetaHabientes Visa (10%) | Selecciona un código promocional | Standard | Masculino | Pool      | Lopez    | Cédula de ciudadania | Cédula de extranjería | Enero-22-1996 | 3154878989  | p@prueba.com | Llegar a Barcelona | calle 90 | 1           | 07-24   | 641 | 5198924302640265 | Confirmación: |