#Author: afoc
Feature: Consultar usuario en gorest
  Yo como usuario
  Necesito consultar usuario en gorest
  Para tener conocimiento de los usuarios en gorest

  Scenario Outline: Consultar usuario de forma exitosa
    Given "Afoc" desea construir la peticion en gorest "https://gorest.co.in"
    When Consume el endPont "/public/v1/users/" con la instrucción GET y el id
      | id   |
      | <id> |
    Then Verifica que la información coincida
      | id   | codigo   | name   | email   | gender   | status   |
      | <id> | <codigo> | <name> | <email> | <gender> | <status> |

    Examples:
      | id   | codigo | name                  | email                           | gender | status |
      | 1326 | 200    | Dhanapati Varrier III | varrier_dhanapati_iii@lynch.org | male   | active |