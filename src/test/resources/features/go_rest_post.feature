#Author: afoc
Feature: Hacer una petición post en gorest
  Yo como usuario
  Necesito crear una petición post en gorest
  Para agregar un usuario en gorest

  Background:
    Given "Afoc" desea construir la peticion en gorest "https://gorest.co.in"

  Scenario Outline: Agregar un usuario de forma exitosa
    When Consume el endPont "/public/v1/users" con la instrucción POST
      | name   | <name>   |
      | email  | <email>  |
      | gender | <gender> |
      | status | <status> |

    Then Verifica que la información coincida <code>
      | name   | email   | gender   | status   |
      | <name> | <email> | <gender> | <status> |

    Examples:
      | code | name                  | gender | email                                  | status |
      | 201  | Brunaaaaasasdfsasddaoo | male   | prassddadaaueaaasbasdoa@pruasaaaeba.com | active |


  Scenario Outline: Agregar una publicacion de forma exitosa
    When Consume el endPont "/public/v1/posts" con la instrucción POST
      | user_id | <user_id> |
      | title   | <title>   |
      | body    | <body>    |

    Then Verifica que la información del post coincida <code>
      | user_id   | title   | body   |
      | <user_id> | <title> | <body> |

    Examples:
      | code | user_id | title      | body        |
      | 201  | 112     | Afadfgadfgf | AAdsaadfAAAA |


  Scenario Outline: Agregar un comentario de forma exitosa
    When Consume el endPont "/public/v1/comments" con la instrucción POST
      | post_id | <post_id> |
      | name    | <name>    |
      | email   | <email>   |
      | body    | <body>    |

    Then Verifica que la información del comentario coincida <code>
      | post_id   | name   | email   | body   |
      | <post_id> | <name> | <email> | <body> |

    Examples:
      | code | post_id | name               | email                       | body         |
      | 201  | 12      | Afdasdaaoaaac Casss | afoaasafdcasda@aefasdsoc.com | Holaasadaaaaa |